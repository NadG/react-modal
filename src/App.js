import React from 'react';
import logo from './logo.svg';
import './App.css';
import useModal from "./useModal";
import Modal from "./Modal";

function App() {
  const {isShowing, toggle} = useModal();
  return (
    <div className="App">
      <button className="button-default" onClick={toggle}>Show Modal</button>
      <Modal
          isShowing={isShowing}
          hide={toggle}
      />
    </div>
  );
}

export default App;
